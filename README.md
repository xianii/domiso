原神自动弹琴人偶
==================

### DOMISO-Genshin

![](logo.png)

### 由于`Gitee`平台功能的缺失，本项目已**停止**在`Gitee`平台的托管更新。

### 目前本项目仅托管与`Github`平台。



### ❤[Github页面](https://github.com/Nigh/DoMiSo-genshin)❤



###  👉[最新版本下载](https://ghproxy.com/https://github.com/Nigh/DoMiSo-genshin/releases/latest/download/DomisoGenshin.zip)👈 



## 官方社群

- 英文社群，`Discord`：https://discord.gg/5PCebykNaC

- 中文社群，`kaiheila`：https://kaihei.co/IWXRLp

@AV64@
